using SOS.WebApi.ConfigureServices;
using SOS.Services.Exception;
using Serilog;
using Serilog.Formatting.Json;
using Serilog.Events;



var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();


builder.Services.AddInfrastructureServices(builder.Configuration);
//validation registration
ConfigureValidation.AddInfrastructureServices(services:builder.Services);

#region addLog

//add log
builder.Host.UseSerilog((ctx, lc) => lc
    //.WriteTo.Console()
    .WriteTo.File(new JsonFormatter(),
        "important-logs.json",
        restrictedToMinimumLevel: LogEventLevel.Error)
    // Add a log file that will be replaced by a new log file each day
    .WriteTo.File("all-daily-.logs",
        rollingInterval: RollingInterval.Day, restrictedToMinimumLevel: LogEventLevel.Error)
    // Set default minimum log level
    .MinimumLevel.Error());

#endregion

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<GlobalExceptionHandlingMiddleware>();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();