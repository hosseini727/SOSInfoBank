﻿using Microsoft.EntityFrameworkCore;
using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;
using SOS.Infrastructure.Repository;
using SOS.Services.Contract;
using System.Reflection;
using Asp.Versioning;
using MediatR;
using SOS.Services.Services;
using SOS.WebApi.Behaviors;


namespace SOS.WebApi.ConfigureServices
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationDbContext>(db => db.UseSqlServer(connectionString));

            services.AddMediatR(cnf => cnf.RegisterServicesFromAssemblies(AppDomain.CurrentDomain.GetAssemblies()));

            //AutoMapper
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

           
            services.AddDbContext<ApplicationDbContext>(
                options => options.UseSqlServer(connectionString));


            services.AddSwaggerGen();

            //Services
            services.AddTransient<IPeopleService, PeopleService>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));


            services.AddScoped<ICustomerRepository, CustomerRepository>();
            //Api Versioning
            services.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.ReportApiVersions = true;
            });

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));


            return services;
        }
    }
}