﻿using FluentValidation;
using SOS.Services.Transaction.PoepleTransaction;
using SOS.Services.Validation.PeopleValidation;

namespace SOS.WebApi.ConfigureServices;

public static class ConfigureValidation
{
    public static IServiceCollection AddInfrastructureServices( IServiceCollection services)
    {
        //Validation
        services.AddTransient<IValidator<AddPeopleCommand>, AddPeopleHandlerValidator>();
        services.AddTransient<IValidator<UpdatePeopleCommand>, UpdatePeopleHandlerValidation>();
        return services;
    }
    
}