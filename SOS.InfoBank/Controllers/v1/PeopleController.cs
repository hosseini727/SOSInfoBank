﻿using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using SOS.Domain.Entities;
using SOS.Services.Contract;
using SOS.Services.Model.People.Request;
// ReSharper disable ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract

namespace SOS.WebApi.Controllers.v1;

[ApiController]
[ApiVersion(1)]
[Route("api/v{version:apiVersion}/[controller]")]
public class PeopleController : ControllerBase
{
    private readonly IPeopleService _peopleService;

    public PeopleController(IPeopleService peopleService)
    {
        _peopleService = peopleService;
    }


    [HttpPost]
    [Route("Add")]
    public async Task<IActionResult> Add([FromBody] AddPeopleRequestModel model)
    {
        var addPeople = await _peopleService.AddPeople(model);
        return addPeople != null ? Created(nameof(Add),addPeople) : BadRequest();
    }

    [HttpGet]
    [Route("GetAll")]
    public async Task<IActionResult> GetPeople()
    {
        var result = await _peopleService.GetPeople();
        return result.Any() ? Ok(result) : NoContent();
    }

    [HttpPut]
    [Route("Update")]
    public async Task<IActionResult> UpdatePeople([FromBody] UpdatePeopleRequestModel model)
    {
        var result = await _peopleService.UpdatePeople(model);
        return result != null ? Ok(result) : NoContent();
    }

    [HttpDelete]
    [Route("{peopleId:int}")]
    public async Task<IActionResult> DeletePeople(int peopleId)
    {
        var result = await _peopleService.DeletePeople(peopleId);
        return NoContent();
    }

    [HttpGet]
    [Route("{peopleId:int}")]
    public async Task<IActionResult> GetPeopleById(int peopleId)
    {
        var result = await _peopleService.GetPeopleById(peopleId);
        return result != null ? Ok(result) : NoContent();
    }
    
}