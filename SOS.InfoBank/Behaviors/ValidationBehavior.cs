
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace SOS.WebApi.Behaviors;

public class ValidationBehavior<Trequest, TResponse> : IPipelineBehavior<Trequest, TResponse> where Trequest : IRequest<TResponse>
{
    private readonly IEnumerable<IValidator<Trequest>> _validators;
    private readonly List<ValidationFailure> _exceptions;

    public ValidationBehavior(IEnumerable<IValidator<Trequest>> validator) => (_validators, _exceptions) = (validator, new List<ValidationFailure>());


    public async Task<TResponse> Handle(Trequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
    {
        if (!_validators.Any())
            return await next();

        var context = new ValidationContext<Trequest>(request);
        var validationFailures = _validators
            .Select(validator => validator.Validate(request))
            .SelectMany(validationResult => validationResult.Errors)
            .Where(validationFailure => validationFailure != null)
            .ToList();

        if (validationFailures.Any())
        {
            foreach (var ex in validationFailures)
            {
                _exceptions.Add(ex);
            }
            var error = string.Join("\r\n", validationFailures);
            throw new ValidationException(error,_exceptions);
        }

        return await next();
    }
}
