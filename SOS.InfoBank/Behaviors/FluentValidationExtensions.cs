using System.Reflection;
using FluentValidation;

namespace SOS.WebApi.Behaviors;

public static class FluentValidationExtensions
{
    public static IServiceCollection AddFluentValidation(this IServiceCollection services, Assembly assembly)
    {
        AssemblyScanner.FindValidatorsInAssembly(assembly)
            .ForEach(result =>
            {
                services.AddScoped(result.InterfaceType, result.ValidatorType);
            });

        return services;
    }
}
