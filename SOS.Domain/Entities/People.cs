﻿namespace SOS.Domain.Entities;

public class People : BaseEntity
{
    #region |Properties|
    
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string NationalCode { get; set; } = string.Empty;
    public DateTime BirthDate { get; set; }
    
    #endregion
    

    #region  |Navigation|

    public virtual ICollection<MemberContractRelation> MemberContractRelations { get; set; } 
        = new HashSet<MemberContractRelation>();
    #endregion
}