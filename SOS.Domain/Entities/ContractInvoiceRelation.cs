﻿namespace SOS.Domain.Entities;

public class ContractInvoiceRelation : BaseEntity
{
    #region |Properties|

    public int ContractId { get; set; }
    public int InvoiceId { get; set; }

    #endregion


    #region |Navigation|

    public virtual Contract? Contract { get; set; }
    public virtual Invoice? Invoice { get; set; }

    #endregion
}