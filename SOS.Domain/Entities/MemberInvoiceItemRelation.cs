﻿namespace SOS.Domain.Entities;

public class MemberInvoiceItemRelation : BaseEntity
{
    #region |Properties|
    public int MemberContractId { get; set; }
    public int InvoiceItemId { get; set; }
    #endregion
    
    #region  |Navigation|

    public virtual InvoiceItem? InvoiceItem { get; set; }
    public virtual MemberContractRelation? MemberContractRelation { get; set; }
    #endregion
}