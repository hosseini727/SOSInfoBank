﻿namespace SOS.Domain.Entities;

public class InvoiceItem : BaseEntity
{
    #region |Properties|

    public int InvoiceId { get; set; }
    public int NetPrice { get; set; }
    public int DeductionPrice { get; set; }
    public int TaxPrice { get; set; }

    #endregion
    
    #region  |Navigation|
    
    public virtual Invoice? Invoice { get; set; }
    public virtual ICollection<MemberInvoiceItemRelation> MemberInvoiceItems { get; set; } 
        = new HashSet<MemberInvoiceItemRelation>();
    #endregion
}