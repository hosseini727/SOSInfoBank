﻿namespace SOS.Domain.Entities;

public class Contract : BaseEntity
{
    #region |Properties|

    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }

    #endregion

    #region |Navigation|

    public virtual ICollection<MemberContractRelation> MemberContractRelations { get; set; } 
        = new HashSet<MemberContractRelation>();
    
    public virtual ICollection<ContractInvoiceRelation> ContractInvoiceRelations { get; set; } 
        = new HashSet<ContractInvoiceRelation>();

    #endregion
}