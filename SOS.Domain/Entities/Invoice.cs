﻿namespace SOS.Domain.Entities;

public class Invoice : BaseEntity
{
    #region |Properties|
    public DateTime InvoiceDate { get; set; }
    public int NetPrice { get; set; }
    #endregion
    
    #region  |Navigation|

    public virtual ICollection<InvoiceItem> InvoiceItems { get; set; } 
        = new HashSet<InvoiceItem>();
    
    public virtual ICollection<ContractInvoiceRelation> ContractInvoiceRelations { get; set; } 
        = new HashSet<ContractInvoiceRelation>();
    #endregion
    
}