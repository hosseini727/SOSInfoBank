﻿namespace SOS.Domain.Entities;

public class MemberContractRelation : BaseEntity
{
    #region |Properties|

    public int PeopleId { get; set; }
    public int ContractId { get; set; }

    #endregion

    #region |Navigation|

    public virtual People? People { get; set; }
    public virtual Contract? Contract { get; set; }
    public virtual ICollection<MemberInvoiceItemRelation> MemberInvoiceItemRelationRelations { get; set; } 
        = new HashSet<MemberInvoiceItemRelation>();

    #endregion
}