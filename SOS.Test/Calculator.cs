﻿namespace SOS.Test
{
 
        public class Calculator
        {
            public int Add(int a, int b)
            {
                return a + b;
            }

            public int Subtract(int a, int b)
            {
                return a - b;
            }

            public int Multiply(int a, int b)
            {
                return a * b;
            }

            public double Divide(int dividend, int divisor)
            {
                if (divisor == 0)
                {
                    throw new ArgumentException("Divisor cannot be zero", nameof(divisor));
                }

                return (double)dividend / divisor;
            }
        }

}