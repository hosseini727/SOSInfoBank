﻿using AutoMapper;
using Moq;
using SOS.Domain.Entities;
using SOS.Infrastructure.Repository.Interface;
using SOS.Services.Model.People.Request;
using SOS.Services.Transaction.PoepleTransaction;
using Xunit;


namespace SOS.Test.HandlerTest;

public class PeopleHandlerTest
{
    private readonly AddPeopleHandle _sut;
    private readonly Mock<IUnitOfWork> _mockUnitOfWork = new();
    private readonly Mock<IPeopleRepository> _mockpeopleRepo = new();
    private readonly Mock<IMapper> _mockMapper = new();

    public PeopleHandlerTest()
    {
        _sut = new AddPeopleHandle(_mockUnitOfWork.Object , _mockMapper.Object);
    }

    [Fact]
    public async Task Handle_ShouldAddPeople_WhenModelIsCorrect()
    {
        //Arrange
        var addPeopleModel = new AddPeopleRequestModel
        {
            FirstName = "Name test",
            LastName = "FamilyTest",
            NationalCode = "00102626",
            BirthDate = new DateTime(2023, 05, 10)
        };
        
        var addPeopleEntity = new People()
        {
            Id = 1,
            FirstName = "Name test",
            LastName = "FamilyTest",
            NationalCode = "0123456789",
            BirthDate = new DateTime(2023, 05, 10)
        };

        var command = new AddPeopleCommand(addPeopleModel);
        _mockMapper.Setup(x => x.Map<People>(addPeopleModel)).Returns(addPeopleEntity);
        _mockUnitOfWork.Setup(x => x.PeopleRepository).Returns(_mockpeopleRepo.Object);
        //_mockpeopleRepo.Setup(x => x.Add(addPeopleEntity)).ReturnsAsync(Peop);
        _mockUnitOfWork.Setup(x => x.CompleteAsync()).ReturnsAsync(true);
        //Action
        var result = await _sut.Handle(command, default);
        //Assert
        // Assert.Empty(result);
       _mockMapper.Verify(x => x.Map<People>(addPeopleModel), Times.Once);
       _mockpeopleRepo.Verify(x => x.Add(addPeopleEntity), Times.Once);
       _mockUnitOfWork.Verify(x => x.CompleteAsync(), Times.Once);
    }

    [Fact]
    public async Task Handle_ShouldThrowArgumentException_WhenNationalCodeIsEmpty()
    {
        // Arrange
        var addPeopleModel = new AddPeopleRequestModel
        {
            FirstName = "Name test",
            LastName = "FamilyTest",
            NationalCode = "", 
            BirthDate = new DateTime(2023, 05, 10)
        };

        var command = new AddPeopleCommand(addPeopleModel);

        // Act and Assert
        await Assert.ThrowsAsync<ArgumentException>(() => _sut.Handle(command, default));
    }

    [Fact]
    public async Task Handle_ShouldThrowArgumentException_WhenNationalCodeLengthIsLessThan10()
    {
        // Arrange
        var addPeopleModel = new AddPeopleRequestModel
        {
            FirstName = "Name test",
            LastName = "FamilyTest",
            NationalCode = "123456789", 
            BirthDate = new DateTime(2023, 05, 10)
        };

        var command = new AddPeopleCommand(addPeopleModel);

        // Act and Assert
        await Assert.ThrowsAsync<ArgumentException>(() => _sut.Handle(command, default));
    }

    [Fact]
    public async Task Handle_ShouldNotThrowArgumentException_WhenNationalCodeLengthIs10()
    {
        // Arrange
        var addPeopleModel = new AddPeopleRequestModel
        {
            FirstName = "Name test",
            LastName = "FamilyTest",
            NationalCode = "1234567890", 
            BirthDate = new DateTime(2023, 05, 10)
        };

        var command = new AddPeopleCommand(addPeopleModel);

        //Action
        var result = await _sut.Handle(command, default);
        //Assert
        //Assert.True(result);
    }


    [Fact]
    public async Task Handle_ShouldThrowArgumentException_WhenFirstNameIsNull()
    {
        // Arrange
        var addPeopleModel = new AddPeopleRequestModel
        {
            FirstName = "", 
            LastName = "LastNameTest", 
            NationalCode = "1234567890",
            BirthDate = new DateTime(2023, 05, 10)
        };

        var command = new AddPeopleCommand(addPeopleModel);

        // Act 
        await Assert.ThrowsAsync<ArgumentException>(() => _sut.Handle(command, default));
    }


    [Fact]
    public async Task Handle_ShouldThrowArgumentException_WhenLastNameIsNull()
    {
        // Arrange
        var addPeopleModel = new AddPeopleRequestModel
        {
            FirstName = "test",
            LastName = "",
            NationalCode = "1234567890",
            BirthDate = new DateTime(2023, 05, 10)
        };

        var command = new AddPeopleCommand(addPeopleModel);

        // Act 
        await Assert.ThrowsAsync<ArgumentException>(() => _sut.Handle(command, default));
    }


    [Fact]
    public async Task Handle_ShouldThrowArgumentException_WhenFirstNameLengthIsLessThan3()
    {
        // Arrange
        var addPeopleModel = new AddPeopleRequestModel
        {
            FirstName = "es", 
            LastName = "LastNameTest", 
            NationalCode = "1234567890",
            BirthDate = new DateTime(2023, 05, 10)
        };

        var command = new AddPeopleCommand(addPeopleModel);

        // Act and Assert
        await Assert.ThrowsAsync<ArgumentException>(() => _sut.Handle(command, default));
    }

    [Fact]
    public async Task Handle_ShouldThrowArgumentException_WhenLastNameLengthIsLessThan3()
    {
        // Arrange
        var addPeopleModel = new AddPeopleRequestModel
        {
            FirstName = "essi",
            LastName = "ho",
            NationalCode = "1234567890",
            BirthDate = new DateTime(2023, 05, 10)
        };

        var command = new AddPeopleCommand(addPeopleModel);

        // Act and Assert
        await Assert.ThrowsAsync<ArgumentException>(() => _sut.Handle(command, default));
    }
}