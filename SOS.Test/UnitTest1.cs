using Xunit;

namespace SOS.Test
{
    public class Tests
    {
        [Fact]
        public void Add_WhenCalled_ReturnsCorrectSum()
        {
            // Arrange
            var calculator = new Calculator();
            int a = 5;
            int b = 3;
            int expectedSum = 8;

            // Act
            int actualSum = calculator.Add(a, b);

            // Assert
            Assert.Equal(expectedSum, actualSum);
        }
    }
}