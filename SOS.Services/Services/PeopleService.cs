﻿using MediatR;
using SOS.Services.Contract;
using SOS.Services.Model.People.Request;
using SOS.Services.Model.People.Response;
using SOS.Services.Transaction.PoepleTransaction;

namespace SOS.Services.Services;

public class PeopleService : IPeopleService
{
    private readonly IMediator _mediator;
    

    public PeopleService(IMediator mediator)
    {
        _mediator = mediator;
    }


    public async Task<AddPeopleResponseModel> AddPeople(AddPeopleRequestModel model)
    {
        var command = new AddPeopleCommand(model);
        var addedPeople = await _mediator.Send(command);
        return addedPeople;
    }

    public async Task<List<GetPeopleResponseModel>> GetPeople()
    {
        var query = new GetPeopleQuery();
        var result = await _mediator.Send(query);
        return result;
    }

    public async Task<GetPeopleResponseModel> UpdatePeople(UpdatePeopleRequestModel updateModel)
    {
        var command = new UpdatePeopleCommand(updateModel);
        var result = await _mediator.Send(command);
        return result;
    }

    public async Task<bool> DeletePeople(int id)
    {
        var command = new DeletePeopleCommand(id);
        var result = await _mediator.Send(command);
        return result;
    }

    public async Task<GetPeopleResponseModel> GetPeopleById(int peopleId)
    {
        var query = new GetPeopleByIdQuery(peopleId);
        var result = await _mediator.Send(query);
        return result;
    }
}