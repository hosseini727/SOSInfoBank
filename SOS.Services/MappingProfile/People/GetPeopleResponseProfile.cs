﻿using AutoMapper;
using SOS.Services.Model.People.Response;

namespace SOS.Services.MappingProfile.People;

public class GetPeopleResponseProfile : Profile
{
    public GetPeopleResponseProfile()
    {
        CreateMap<Domain.Entities.People, GetPeopleResponseModel>()
            .ForMember(des => des.FullName, opt =>
                opt.MapFrom(src => src.FirstName + " " + src.LastName))
            .ForMember(des => des.RegisterDate, opt =>
                opt.MapFrom(src => src.CreatedDate));
    }
}