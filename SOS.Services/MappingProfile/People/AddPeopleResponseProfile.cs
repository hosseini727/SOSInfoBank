﻿using AutoMapper;
using SOS.Services.Model.People.Response;

namespace SOS.Services.MappingProfile.People;

public class AddPeopleResponseProfile : Profile
{
    public AddPeopleResponseProfile()
    {
        CreateMap<Domain.Entities.People, AddPeopleResponseModel>()
            .ForMember(des => des.Id,
                opt => opt.MapFrom(src => src.Id))
            .ForMember(des => des.FullName,
                opt => opt.MapFrom(
                    src => src.FirstName + " " + src.LastName
                ));
    }
}