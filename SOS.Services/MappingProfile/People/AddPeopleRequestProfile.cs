﻿using AutoMapper;
using SOS.Services.Model.People.Request;

namespace SOS.Services.MappingProfile.People;

public class AddPeopleRequestProfile : Profile
{
    public AddPeopleRequestProfile()
    {
        CreateMap<AddPeopleRequestModel, Domain.Entities.People>()
            .ForMember(des => des.CreatedDate, opt=>opt.MapFrom(src=>DateTime.Now));
    }
}