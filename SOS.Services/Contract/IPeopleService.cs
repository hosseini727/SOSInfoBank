﻿using SOS.Services.Model.People.Request;
using SOS.Services.Model.People.Response;

namespace SOS.Services.Contract;

public interface IPeopleService
{
    Task<AddPeopleResponseModel> AddPeople(AddPeopleRequestModel model);
    Task<List<GetPeopleResponseModel>> GetPeople();
    Task<GetPeopleResponseModel> UpdatePeople(UpdatePeopleRequestModel updateModel);
    Task<bool> DeletePeople(int id);
    Task<GetPeopleResponseModel> GetPeopleById(int id);
}