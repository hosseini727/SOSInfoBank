﻿using System.Data;
using FluentValidation;
using SOS.Services.Transaction.PoepleTransaction;

namespace SOS.Services.Validation.PeopleValidation;

public class UpdatePeopleHandlerValidation : AbstractValidator<UpdatePeopleCommand>
{
    public UpdatePeopleHandlerValidation()
    {
        //RuleFor(x => x.UpdatePeopleCommand.FirstName).NotEmpty().MinimumLength(3);
        RuleFor(x => x.UpdateModel.FirstName).NotEmpty().MinimumLength(3);
        RuleFor(x => x.UpdateModel.LastName).NotEmpty().MinimumLength(3);
        RuleFor(x => x.UpdateModel.NationalCode).NotEmpty().Length(10);
        RuleFor(x => x.UpdateModel.BirthDate).NotEmpty();
    }
}