﻿using FluentValidation;
using SOS.Services.Transaction.PoepleTransaction;

namespace SOS.Services.Validation.PeopleValidation;

public class AddPeopleHandlerValidator : AbstractValidator<AddPeopleCommand>
{
    public AddPeopleHandlerValidator()
    {
        RuleFor(x => x.AddPeopleModel.FirstName).NotEmpty().MinimumLength(3);
        RuleFor(x => x.AddPeopleModel.LastName).NotEmpty().MinimumLength(3);
        RuleFor(x => x.AddPeopleModel.NationalCode).NotEmpty().Length(10);
        RuleFor(x => x.AddPeopleModel.BirthDate).NotEmpty();
    }
}