﻿using AutoMapper;
using MediatR;
using SOS.Infrastructure.Repository.Interface;
using SOS.Services.Model.People.Response;

namespace SOS.Services.Transaction.PoepleTransaction;

public class GetPeopleHandler : IRequestHandler<GetPeopleQuery,List<GetPeopleResponseModel>>
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;

    public GetPeopleHandler(IMapper mapper , IUnitOfWork unitOfWork)
    {
        _mapper = mapper;
        _unitOfWork = unitOfWork;
    }
    public async Task<List<GetPeopleResponseModel>> Handle(GetPeopleQuery request, CancellationToken cancellationToken)
    {
        var result = await _unitOfWork.PeopleRepository.GetAll();
        var response = _mapper.Map<List<GetPeopleResponseModel>>(result);
        return response;
    }
}