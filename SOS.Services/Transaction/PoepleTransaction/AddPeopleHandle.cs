﻿using AutoMapper;
using MediatR;
using SOS.Infrastructure.Repository.Interface;
using SOS.Services.Model.People.Response;

namespace SOS.Services.Transaction.PoepleTransaction;

public class AddPeopleHandle : IRequestHandler<AddPeopleCommand, AddPeopleResponseModel>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public AddPeopleHandle(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public async Task<AddPeopleResponseModel> Handle(AddPeopleCommand request, CancellationToken cancellationToken)
    {
        var people = _mapper.Map<Domain.Entities.People>(request.AddPeopleModel);
        var insertPeople = await _unitOfWork.PeopleRepository.Add(people);
        {
            await _unitOfWork.CompleteAsync();
            var response = _mapper.Map<AddPeopleResponseModel>(insertPeople);
            return response;
        }
    }
}