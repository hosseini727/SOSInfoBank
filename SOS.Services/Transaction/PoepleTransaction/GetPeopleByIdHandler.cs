﻿using AutoMapper;
using MediatR;
using SOS.Domain.Entities;
using SOS.Infrastructure.Repository.Interface;
using SOS.Services.Model.People.Response;

namespace SOS.Services.Transaction.PoepleTransaction;

public class GetPeopleByIdHandler : IRequestHandler<GetPeopleByIdQuery , GetPeopleResponseModel>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public GetPeopleByIdHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public async Task<GetPeopleResponseModel> Handle(GetPeopleByIdQuery request, CancellationToken cancellationToken)
    {
        var result = await _unitOfWork.PeopleRepository.GetById(request.PeopleId);
        if (result!=null)
        {
            var people = _mapper.Map<GetPeopleResponseModel>(result);
            return people;
        }

        return null;
    }
}