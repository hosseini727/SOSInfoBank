﻿using MediatR;

namespace SOS.Services.Transaction.PoepleTransaction;

public class DeletePeopleCommand : IRequest<bool>
{
    public int PeopleId;

    public DeletePeopleCommand(int id)
    {
        PeopleId = id;
    }
}