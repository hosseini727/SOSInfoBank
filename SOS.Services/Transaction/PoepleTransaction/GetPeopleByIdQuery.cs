﻿using MediatR;
using SOS.Services.Model.People.Response;

namespace SOS.Services.Transaction.PoepleTransaction;

public class GetPeopleByIdQuery : IRequest<GetPeopleResponseModel>
{
    public int PeopleId;

    public GetPeopleByIdQuery(int id)
    {
        PeopleId = id;
    }
}