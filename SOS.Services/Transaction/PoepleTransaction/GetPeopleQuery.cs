﻿using MediatR;
using SOS.Services.Model.People.Response;

namespace SOS.Services.Transaction.PoepleTransaction;

public class GetPeopleQuery : IRequest<List<GetPeopleResponseModel>>
{
    
}