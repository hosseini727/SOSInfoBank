﻿using AutoMapper;
using MediatR;
using SOS.Infrastructure.Repository.Interface;
using SOS.Services.Model.People.Response;

namespace SOS.Services.Transaction.PoepleTransaction;

public class UpdatePeopleHandler : IRequestHandler<UpdatePeopleCommand , GetPeopleResponseModel>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public UpdatePeopleHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public async Task<GetPeopleResponseModel> Handle(UpdatePeopleCommand request, CancellationToken cancellationToken)
    {
        var existPeople = await _unitOfWork.PeopleRepository.GetById(request.UpdateModel.Id);
        if (existPeople==null)
        {
            return null;
        }

        existPeople.FirstName = request.UpdateModel.FirstName;
        existPeople.LastName = request.UpdateModel.LastName;
        existPeople.NationalCode = request.UpdateModel.NationalCode;
        existPeople.BirthDate = request.UpdateModel.BirthDate;

        _unitOfWork.PeopleRepository.Update(existPeople);
        await _unitOfWork.CompleteAsync();

        var updatedPeople = _mapper.Map<GetPeopleResponseModel>(existPeople);
        return updatedPeople;
    }
}