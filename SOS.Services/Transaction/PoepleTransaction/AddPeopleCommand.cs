﻿using MediatR;
using SOS.Services.Model.People.Request;
using SOS.Services.Model.People.Response;

namespace SOS.Services.Transaction.PoepleTransaction;

public class AddPeopleCommand : IRequest<AddPeopleResponseModel>
{
    public AddPeopleRequestModel AddPeopleModel { get; }

    public AddPeopleCommand(AddPeopleRequestModel addPeopleRequestModel)
    {
        AddPeopleModel = addPeopleRequestModel;
    }
}