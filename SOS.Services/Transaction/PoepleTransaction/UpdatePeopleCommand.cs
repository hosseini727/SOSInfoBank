﻿using MediatR;
using SOS.Services.Model.People.Request;
using SOS.Services.Model.People.Response;

namespace SOS.Services.Transaction.PoepleTransaction;

public class UpdatePeopleCommand : IRequest<GetPeopleResponseModel>
{
    public UpdatePeopleRequestModel UpdateModel;
    
    public UpdatePeopleCommand(UpdatePeopleRequestModel updateModel)
    {
        UpdateModel = updateModel;
    }

}