﻿using MediatR;
using SOS.Infrastructure.Repository.Interface;

namespace SOS.Services.Transaction.PoepleTransaction;

public class DeletePeopleHandler : IRequestHandler<DeletePeopleCommand,bool>
{
    private readonly IUnitOfWork _unitOfWork;

    public DeletePeopleHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<bool> Handle(DeletePeopleCommand request, CancellationToken cancellationToken)
    {
        var result = await _unitOfWork.PeopleRepository.Delete(request.PeopleId);
        if (result == null)
            return false;
        await _unitOfWork.CompleteAsync();
        return true;
    }
}