namespace SOS.Services.Exception;

public class NotImplementedException : System.Exception
{
    public NotImplementedException(string msg) : base(msg)
    { }
}
