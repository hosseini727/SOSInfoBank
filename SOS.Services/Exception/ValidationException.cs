namespace SOS.Services.Exception;

public class ValidationException : System.Exception
{
    public ValidationException(string msg) : base(msg)
    {

    }
}
