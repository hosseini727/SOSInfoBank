namespace SOS.Services.Exception;

public class KeyNotFoundException : System.Exception
{
    public KeyNotFoundException(string msg) : base(msg)
    { }
}
