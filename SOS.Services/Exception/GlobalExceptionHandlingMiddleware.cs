
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Text.Json;


namespace SOS.Services.Exception
{
    public class GlobalExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<GlobalExceptionHandlingMiddleware> _logger;
        public GlobalExceptionHandlingMiddleware(RequestDelegate next, ILogger<GlobalExceptionHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (System.Exception exception)
            {         

                await HandleExceptionAsync(exception, httpContext, _logger);
            }
        }

        private static Task HandleExceptionAsync(System.Exception exception, HttpContext context , ILogger<GlobalExceptionHandlingMiddleware> logger)
        {
            HttpStatusCode status;
            string stackTrace = exception.StackTrace ?? string.Empty;
            string message ;
            string exceptionResult;
            var exceptionType = exception.GetType();
            var errorDetails = new ErrorDetails();
            if (exceptionType == typeof(BadRequestException))
            {
                message = exception.Message;
                status = HttpStatusCode.BadRequest;
                stackTrace = exception.StackTrace;
            }
            else if (exceptionType == typeof(NotFoundException))
            {
                message = exception.Message;
                status = HttpStatusCode.NotFound;
                stackTrace = exception.StackTrace;
            }
            else if (exceptionType == typeof(NotImplementedException))
            {
                message = exception.Message;
                status = HttpStatusCode.NotImplemented;
                stackTrace = exception.StackTrace;
            }
            else if (exceptionType == typeof(KeyNotFoundException))
            {
                message = exception.Message;
                status = HttpStatusCode.NotFound;
                stackTrace = exception.StackTrace;
            }
            else if (exceptionType == typeof(UnUthorizedException))
            {
                message = exception.Message;
                status = HttpStatusCode.Unauthorized;
                stackTrace = exception.StackTrace;
            }
            else if (exceptionType == typeof(FluentValidation.ValidationException))
            {
                message = exception.Message;
                status = HttpStatusCode.BadRequest;
                //stackTrace = exception.StackTrace;
            }
            else
            {
                message = exception.Message;
                status = HttpStatusCode.InternalServerError;
                stackTrace = exception.StackTrace;
            }

            errorDetails.ErrorMessage = message;
            errorDetails.StackTrace = stackTrace;
            errorDetails.Errors = GetAdditionalDetails(exception);

            exceptionResult = JsonSerializer.Serialize(errorDetails);

            //add log 
            logger.LogError(exceptionResult);

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)status;

            return context.Response.WriteAsync(exceptionResult);
        }

        private static List<KeyValuePair<string, string>> GetAdditionalDetails(System.Exception exception)
        {
            var additionalDetails = new List<KeyValuePair<string, string>>();
            if (exception is FluentValidation.ValidationException validationException)
            {
                foreach (var error in validationException.Errors)
                {                
                    additionalDetails.Add(new KeyValuePair<string, string>($"{error.PropertyName}", $"{error.ErrorMessage}"));
                }
            }
            return additionalDetails;
        }
    }

    public class ErrorDetails
    {
        public string ErrorMessage { get; set; } = string.Empty;
        public string? StackTrace { get; set; } = string.Empty;
        public List<KeyValuePair<string, string>>? Errors { get; set; }
    }
}