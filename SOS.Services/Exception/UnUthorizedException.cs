namespace SOS.Services.Exception;

public class UnUthorizedException : System.Exception
{
    public UnUthorizedException(string msg) : base(msg)
    { }
}
