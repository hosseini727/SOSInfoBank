﻿namespace SOS.Services.Model.People.Response;

public record GetPeopleResponseModel
{
    public int Id { get; set; }
    public string FullName { get; set; } = string.Empty;
    public string NationalCode { get; set; } = string.Empty;
    public DateTime BirthDate { get; set; }
    public DateTime RegisterDate { get; set; }
}