﻿namespace SOS.Services.Model.People.Response;

public record AddPeopleResponseModel
{
    public int Id { get; set; }
    public string FullName { get; set; } = string.Empty;
}