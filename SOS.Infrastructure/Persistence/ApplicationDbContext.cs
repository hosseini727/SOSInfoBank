﻿using Microsoft.EntityFrameworkCore;
using SOS.Domain.Entities;
namespace SOS.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext
    {

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<People> People { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<MemberContractRelation> MemberContractRelations { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<InvoiceItem> InvoiceItems { get; set; }
        public virtual DbSet<MemberInvoiceItemRelation> MemberInvoiceItemRelations { get; set; }
        public virtual DbSet<ContractInvoiceRelation> ContractInvoiceRelations { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }
    }
}
