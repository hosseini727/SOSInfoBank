﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace SOS.Infrastructure.SharedModel;

public class OperationResult
{
    public bool IsSuccess { get; set; }

    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public string Message { get; set; }

    public static implicit operator OperationResult(ContentResult result)
    {
        return new OperationResult { IsSuccess = true, Message = result.Content };   
    }

    public static implicit operator OperationResult(OkResult result)
    {
        return new OperationResult { IsSuccess = true, Message = "Success" };
    }


    public static implicit operator OperationResult(BadRequestResult result)
    {
        return new OperationResult { IsSuccess = false, Message = "BadRequest" };
    }

}

public class OperationResult<TData> : OperationResult
{

    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public TData Data { get; set; }

    public static implicit operator OperationResult<TData>(TData data)
    {
        return new OperationResult<TData> { IsSuccess = true , Message = "Success" , Data = data };
    }

    public static implicit operator OperationResult<TData>(OkObjectResult result)
    {
        return new OperationResult<TData> { IsSuccess = false, Message = "Success", Data = (TData)result.Value }; 
    }
    

}

