﻿using SOS.Domain.Entities;
using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;

namespace SOS.Infrastructure.Repository;

public class InvoiceContractRepository : GenericRepository<ContractInvoiceRelation> , IInvoiceContractRelationRepository
{
    public InvoiceContractRepository(ApplicationDbContext context) : base(context)
    {
        
    }
}