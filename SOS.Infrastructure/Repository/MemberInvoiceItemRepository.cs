﻿using SOS.Domain.Entities;
using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;

namespace SOS.Infrastructure.Repository;

public class MemberInvoiceItemRepository : GenericRepository<MemberInvoiceItemRelation> , IMemberInvoiceItemRelationRepository
{
    public MemberInvoiceItemRepository(ApplicationDbContext context) : base(context)
    {
        
    }
}