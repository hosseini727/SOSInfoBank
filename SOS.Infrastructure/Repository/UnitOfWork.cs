using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;


namespace SOS.Infrastructure.Repository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {

        private readonly ApplicationDbContext _context;
        public  ICustomerRepository CustomerRepository {get;}
        public IPeopleRepository PeopleRepository { get; }
        public IContractRepository ContractRepository { get; }
        public IMemberContractRelationRepository MemberContractRelationRepository { get; }
        public IInvoiceRepository InvoiceRepository { get; }
        public IInvoiceItemRepository InvoiceItemRepository { get; }
        public IMemberInvoiceItemRelationRepository MemberInvoiceItemRelationRepository { get; }
        public IInvoiceContractRelationRepository InvoiceContractRelationRepository { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            CustomerRepository = new CustomerRepository(_context);
            PeopleRepository = new PeopleRepository(_context);
            ContractRepository = new ContractRepository(_context);
            MemberContractRelationRepository = new MemberContractRepository(_context);
            InvoiceRepository = new InvoiceRepository(_context);
            InvoiceItemRepository = new InvoiceItemRepository(_context);
            MemberInvoiceItemRelationRepository = new MemberInvoiceItemRepository(_context);
            InvoiceContractRelationRepository = new InvoiceContractRepository(_context);
        }

       
        public async Task<bool> CompleteAsync()
        {
            var result = await _context.SaveChangesAsync();
            return result > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}