using Microsoft.EntityFrameworkCore;
using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;

namespace SOS.Infrastructure.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly ApplicationDbContext _context;
        internal DbSet<T> _dbset;

        public GenericRepository(ApplicationDbContext context)
        {
            _context = context;
            _dbset = _context.Set<T>();
        }

        public async Task<T> Add(T entity)
        {
            await _dbset.AddAsync(entity);
            return entity;
        }

        public async Task<T> Delete(long id)
        {
            var entity = await _dbset.FindAsync(id);
            if (entity == null)
            {
                return null!;
            }

            _dbset.Remove(entity);
            return entity;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _dbset.ToListAsync();
        }

        public async Task<T?> GetById(long id)
        {
            return await _dbset.FindAsync(id);
        }

        public virtual T Update(T entity)
        {
            _dbset.Update(entity);
            return entity;
        }
    }
}