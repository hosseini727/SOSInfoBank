﻿using SOS.Domain.Entities;
using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;

namespace SOS.Infrastructure.Repository;

public class ContractRepository : GenericRepository<Contract> , IContractRepository
{
    public ContractRepository(ApplicationDbContext context) : base(context)
    {
        
    }
}