﻿using SOS.Domain.Entities;
using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;

namespace SOS.Infrastructure.Repository;

public class MemberContractRepository : GenericRepository<MemberContractRelation> , IMemberContractRelationRepository
{
    public MemberContractRepository(ApplicationDbContext context) : base(context)
    {
        
    }
}