﻿using SOS.Domain.Entities;
using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;

namespace SOS.Infrastructure.Repository;

public class InvoiceRepository : GenericRepository<Invoice> , IInvoiceRepository
{
    public InvoiceRepository(ApplicationDbContext context) : base(context)
    {
        
    }
}