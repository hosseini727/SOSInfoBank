﻿using SOS.Domain.Entities;

namespace SOS.Infrastructure.Repository.Interface;

public interface IPeopleRepository : IGenericRepository<People>
{
    
}