namespace SOS.Infrastructure.Repository.Interface
{
    public interface IUnitOfWork
    {
        ICustomerRepository CustomerRepository { get; }
        IPeopleRepository PeopleRepository { get; }
        IContractRepository ContractRepository { get; }
        IMemberContractRelationRepository MemberContractRelationRepository { get; }
        IInvoiceRepository InvoiceRepository { get; }
        IInvoiceItemRepository InvoiceItemRepository { get; }
        IMemberInvoiceItemRelationRepository MemberInvoiceItemRelationRepository { get; }
        IInvoiceContractRelationRepository InvoiceContractRelationRepository { get; }
        Task<bool> CompleteAsync();
    }
}