using SOS.Domain.Entities;

namespace SOS.Infrastructure.Repository.Interface;
public interface ICustomerRepository : IGenericRepository<Customer>
{
    Task<IEnumerable<Customer>> GetTenLastCustormers();
}
