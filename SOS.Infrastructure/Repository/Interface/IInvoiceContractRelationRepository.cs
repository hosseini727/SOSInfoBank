﻿using SOS.Domain.Entities;

namespace SOS.Infrastructure.Repository.Interface;

public interface IInvoiceContractRelationRepository : IGenericRepository<ContractInvoiceRelation>
{
    
}