using Microsoft.EntityFrameworkCore;
using SOS.Domain.Entities;
using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;

namespace SOS.Infrastructure.Repository
{
    public class CustomerRepository : GenericRepository<Customer> , ICustomerRepository
    {
        public CustomerRepository(ApplicationDbContext context) : base(context)
        {
            
        }

       public async Task<IEnumerable<Customer>> GetTenLastCustormers(){
            var result = await _dbset.ToListAsync();
            return result;
       }

    }
}