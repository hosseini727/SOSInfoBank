﻿using SOS.Domain.Entities;
using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;

namespace SOS.Infrastructure.Repository;

public class InvoiceItemRepository : GenericRepository<InvoiceItem> , IInvoiceItemRepository
{
    public InvoiceItemRepository(ApplicationDbContext context) : base(context)
    {
        
    }
}