﻿using SOS.Domain.Entities;
using SOS.Infrastructure.Persistence;
using SOS.Infrastructure.Repository.Interface;

namespace SOS.Infrastructure.Repository;

public class PeopleRepository : GenericRepository<People> , IPeopleRepository
{
    public PeopleRepository(ApplicationDbContext context) : base(context)
    {
        
    }
}